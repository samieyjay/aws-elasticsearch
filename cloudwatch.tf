resource "aws_cloudwatch_log_group" "elasticsearch" {
  count = length(var.log_type) > 0 ? 1 : 0
  name  = "${var.name}-log_group"

  tags = {
    Name = "${var.name}-log_group"
  }
}

resource "aws_cloudwatch_log_resource_policy" "elasticsearch" {
  count       = length(var.log_type) > 0 ? 1 : 0
  policy_name = "${var.name}-policy"

  policy_document = <<CONFIG
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "es.amazonaws.com"
      },
      "Action": [
        "logs:PutLogEvents",
        "logs:PutLogEventsBatch",
        "logs:CreateLogStream"
      ],
      "Resource": "arn:aws:logs:*"
    }
  ]
}
CONFIG
}