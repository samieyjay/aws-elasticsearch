variable "name" {
  type        = string
  description = "Name of the elasticsearch cluster"
}

variable "tags" {
  type    = map(string)
  default = {}
}

variable "environment" {
  description = "The environment the elasticsearch cluster is running in i.e. dev, prod etc"
}

variable "create_vpc_access" {
  type        = string
  default     = null
  description = "Enable vpc access for elasticsearch"
}

variable "vpc_cidr" {
  default     = ""
  description = "cidr range for the vpc"
}

variable "vpc_id" {
  type        = string
  default     = ""
  description = "vpc id of already existing vpc"
}

variable "subnets" {
  default     = []
  description = "cidr range for the vpc"
}

variable "subnet_ids" {
  default     = []
  description = "subnet ids of already existing subnets"
}

variable "sg_cidr" {
  default     = ["0.0.0.0/0"]
  description = "cidr range for the vpc"
}

variable "elasticsearch_version" {
  type        = string
  default     = "7.1"
  description = "Version of Elasticsearch to deploy"
}

variable "instance_type" {
  type        = string
  default     = "r5.large.elasticsearch"
  description = "Elasticsearch instance type for data nodes in the cluster"
}

variable "instance_count" {
  description = "Number of data nodes in the cluster"
  default     = 3
}

variable "zone_awareness_enabled" {
  type        = string
  default     = "false"
  description = "Enable zone awareness for Elasticsearch cluster"
}

variable "availability_zone_count" {
  type        = number
  default     = 2
  description = "Number of Availability Zones for the domain to use with zone_awareness_enabled"
}

variable "ebs_volume_size" {
  description = "Optionally use EBS volumes for data storage by specifying volume size in GB"
  default     = 0
}

variable "ebs_volume_type" {
  type        = string
  default     = "gp2"
  description = "Storage type of EBS volumes"
}

variable "ebs_iops" {
  default     = 0
  description = "The baseline input/output (I/O) performance of EBS volumes attached to data nodes. Applicable only for the Provisioned IOPS EBS volume type"
}

variable "encrypt_at_rest_enabled" {
  type        = string
  default     = "false"
  description = "Whether to enable encryption at rest"
}

variable "log_type" {
  type        = list
  default     = []
  description = "Specifies type of Elasticsearch log"
}

variable "automated_snapshot_start_hour" {
  description = "Hour at which automated snapshots are taken, in UTC"
  default     = 0
}

variable "dedicated_master_enabled" {
  type        = string
  default     = "false"
  description = "Indicates whether dedicated master nodes are enabled for the cluster"
}

variable "dedicated_master_count" {
  description = "Number of dedicated master nodes in the cluster"
  default     = 0
}

variable "dedicated_master_type" {
  type        = string
  default     = "t2.small.elasticsearch"
  description = "Instance type of the dedicated master nodes in the cluster"
}

variable "node_to_node_encryption_enabled" {
  type        = string
  default     = "false"
  description = "Whether to enable node-to-node encryption"
}