Terraform module to deploy Elasticsearch in 3 major ways

#### Option 1

Deploy Elasticsearch domain with public endpoint access.

Usage:

```
module elasticsearch {
    source = "./elasticsearch"
    name = "dsa-mma-elasticsearch"
    elasticsearch_version = "7.1"
    encrypt_at_rest_enabled = true
    node_to_node_encryption_enabled = true
    dedicated_master_enabled = true
    dedicated_master_count = 3
    dedicated_master_type = "r5.large.elasticsearch"
    instance_count = 3
    instance_type = "r5.large.elasticsearch"
    ebs_volume_type = "gp2"
    ebs_volume_size = 40
    environment = "dev"
}
```

#### Option 2

Deploy Elasticsearch using into a VPC by supplying existing VPC (vpc_ids) and subnet values (subnet_ids).

Usage:

```
module elasticsearch {
    source = "./elasticsearch"
    name = "dsa-mma-elasticsearch"
    elasticsearch_version = "7.1"
    create_vpc_access = "manual"
    vpc_id = "vpc-xxx"
    subnet_ids = ["subnet-xxx", "subnet-xxx", "subnet-xxx"]
    encrypt_at_rest_enabled = true
    node_to_node_encryption_enabled = true
    dedicated_master_enabled = true
    dedicated_master_count = 3
    dedicated_master_type = "r5.large.elasticsearch"
    instance_count = 3
    instance_type = "r5.large.elasticsearch"
    ebs_volume_type = "gp2"
    ebs_volume_size = 40
    zone_awareness_enabled = true
    availability_zone_count = 3
    environment = "dev"
}
```
#### Option 3

Deploy Elasticsearch domain into a VPC and create required VPC resources. Set create_vpc_access variable to "auto" and provide your CIDR value for the new VPC (vpc_cidr), including CIDR values for subnets (subnets).

Usage:

```
module elasticsearch {
    source = "./elasticsearch"
    name = "dsa-mma-elasticsearch"
    elasticsearch_version = "7.1"
    create_vpc_access = "auto"
    vpc_cidr = "192.168.0.0/22"
    subnets = ["192.168.0.0/26", "192.168.0.64/26", "192.168.0.128/26"]
    encrypt_at_rest_enabled = true
    node_to_node_encryption_enabled = true
    dedicated_master_enabled = true
    dedicated_master_count = 3
    dedicated_master_type = "r5.large.elasticsearch"
    instance_count = 3
    instance_type = "r5.large.elasticsearch"
    ebs_volume_type = "gp2"
    ebs_volume_size = 40
    zone_awareness_enabled = true
    availability_zone_count = 3
    environment = "dev"
}
```

Note (Applicable to options 2 and 3): Default value of availability_zone_count is 2 when zone_awareness_enabled is set to true. If an availability_zone_count option value is set to 3, you must provide at least 3 subnet values

#### Logging

Provide single or multiple values for log_type to enable each log type

```
...
log_type = ["INDEX_SLOW_LOGS", "SEARCH_SLOW_LOGS", "ES_APPLICATION_LOGS"]
...
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| automated\_snapshot\_start\_hour | Hour at which automated snapshots are taken, in UTC | string | `"0"` | no |
| availability\_zone\_count | Number of Availability Zones for the domain to use with zone\_awareness\_enabled. Defaults to 2. Valid values: 2 or 3 | string | `"false"` | no |
| create\_vpc\_access | Indicates whether to enable vpc access for cluster. Valid values: manual or auto. Auto requires 2 variables: vpc\_cidr and subnets. Manual requires 2 variables: vpc_id and subnet_ids. Also accepts sg_cidr as optional variable for both | string | n/a | no |
| dedicated\_master\_count | Number of dedicated master nodes in the cluster | string | `"0"` | no |
| dedicated\_master\_enabled | Indicates whether dedicated master nodes are enabled for the cluster | string. Accepts dedicated\_master\_count and dedicated\_master\_type as optional variables | `"false"` | no |
| dedicated\_master\_type | Instance type of the dedicated master nodes in the cluster | string | `"r5.large.elasticsearch"` | no |
| ebs\_iops | The baseline input/output (I/O) performance of EBS volumes attached to data nodes. Applicable only for the Provisioned IOPS EBS volume type | string | `"0"` | no |
| ebs\_volume\_size | Optionally use EBS volumes for data storage by specifying volume size in GB | string | `"0"` | no |
| ebs\_volume\_type | Storage type of EBS volumes | string | `"gp2"` | no |
| elasticsearch\_version | Version of Elasticsearch to deploy | string | `"6.3"` | no |
| encrypt\_at\_rest\_enabled | Whether to enable encryption at rest | string | `"false"` | no |
| environment | The environment the elasticsearch cluster is running in i.e. dev, prod etc | string | n/a | yes |
| instance\_count | Number of data nodes in the cluster | string | `"4"` | no |
| instance\_type | Elasticsearch instance type for data nodes in the cluster | string | `"r5.large.elasticsearch"` | no |
| log\_type | Specifies whether log publishing option is enabled or not. Valid values INDEX_SLOW_LOGS, SEARCH_SLOW_LOGS, ES_APPLICATION_LOGS | list | `[]` | no |
| name | Name of the elasticsearch cluster | string | n/a | yes |
| node\_to\_node\_encryption\_enabled | Whether to enable node-to-node encryption | string | `"false"` | no |
| sg\_cidr | List of cidr range to grant access from the security group | list | ["0.0.0.0/0"] | no |
| subnets | cidr range of new subnets - Required when create\_vpc\_access is set to manual| list | `[]` | no |
| subnet\_ids | IDs of existing subnets - Required when create\_vpc\_access is set to manual | list | `[]` | no |
| tags |  | map | `<map>` | no |
| vpc\_cidr | cidr range of new vpc - Required when create\_vpc\_access is set to manual| string | n/a | no |
| vpc\_id | ID of already existing vpc - Required when create\_vpc\_access is set to manual | string | n/a | no |
| zone\_awareness\_enabled | Indicates whether zone awareness is enabled | string | `"false"` | no |

## Outputs

| Name | Description |
|------|-------------|
| domain\_arn | ARN of the Elasticsearch domain |
| domain\_endpoint | Domain-specific endpoint used to submit index, search, and data upload requests |
| domain\_id | Unique identifier for the Elasticsearch domain |
| kibana\_endpoint | Domain-specific endpoint for kibana without https scheme |
