terraform {
  required_version = ">= 0.12"
}

resource "aws_elasticsearch_domain" "elasticsearch" {
  domain_name           = var.name
  elasticsearch_version = var.elasticsearch_version

  cluster_config {
    instance_count           = var.instance_count
    instance_type            = var.instance_type
    dedicated_master_enabled = var.dedicated_master_enabled
    dedicated_master_count   = var.dedicated_master_count
    dedicated_master_type    = var.dedicated_master_type
    zone_awareness_enabled   = var.zone_awareness_enabled

    dynamic "zone_awareness_config" {
      for_each = var.zone_awareness_enabled == "false" ? [] : [var.zone_awareness_enabled]
      content {
        availability_zone_count = var.availability_zone_count
      }
    }
  }

  ebs_options {
    ebs_enabled = var.ebs_volume_size > 0 ? true : false
    volume_size = var.ebs_volume_size
    volume_type = var.ebs_volume_type
  }

  dynamic "vpc_options" {
    for_each = var.create_vpc_access == "auto" || var.create_vpc_access == "manual" ? [var.create_vpc_access] : []
    content {
      subnet_ids         = var.create_vpc_access == "auto" ? aws_subnet.elasticsearch.*.id : var.subnet_ids
      security_group_ids = [aws_security_group.elasticsearch_sg[0].id]
    }
  }

  encrypt_at_rest {
    enabled = var.encrypt_at_rest_enabled
  }

  node_to_node_encryption {
    enabled = var.node_to_node_encryption_enabled
  }

  snapshot_options {
    automated_snapshot_start_hour = var.automated_snapshot_start_hour
  }

  dynamic log_publishing_options {
    for_each = var.log_type
    content {
      log_type                 = log_publishing_options.value
      cloudwatch_log_group_arn = aws_cloudwatch_log_group.elasticsearch[0].arn
    }
  }

  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s", var.environment, var.name)
    },
    {
      "Env" = var.environment
    },
  )
}

resource "aws_iam_user" "elasticsearch_iam_user" {
  name = "${var.name}-iam-user"
  path = "/"
}

resource "aws_elasticsearch_domain_policy" "elasticsearch" {
  domain_name = aws_elasticsearch_domain.elasticsearch.domain_name

  access_policies = <<POLICIES
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "es:*",
            "Principal": { "AWS": "${aws_iam_user.elasticsearch_iam_user.arn}" },
            "Effect": "Allow",
            "Resource": "${aws_elasticsearch_domain.elasticsearch.arn}/*"
        }
    ]
}
POLICIES

}