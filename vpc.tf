data "aws_availability_zones" "available" {}

resource "aws_vpc" "elasticsearch" {
  count      = var.create_vpc_access == "auto" ? 1 : 0
  cidr_block = var.vpc_cidr

  tags = {
    Name = var.name
  }
}

resource "aws_subnet" "elasticsearch" {
  count             = var.create_vpc_access == "auto" ? length(var.subnets) : 0
  vpc_id            = aws_vpc.elasticsearch[0].id
  cidr_block        = element(var.subnets, count.index)
  availability_zone = element(data.aws_availability_zones.available.names, count.index)

  tags = {
    Name = var.name
  }
}

resource "aws_route_table" "elasticsearch" {
  count  = var.create_vpc_access == "auto" ? 1 : 0
  vpc_id = aws_vpc.elasticsearch[0].id

  tags = {
    Name = "${var.name}-privatert"
  }
}

resource "aws_route_table_association" "elasticsearch" {
  count          = var.create_vpc_access == "auto" ? length(var.subnets) : 0
  subnet_id      = element(aws_subnet.elasticsearch.*.id, count.index)
  route_table_id = aws_route_table.elasticsearch[0].id
}

resource "aws_security_group" "elasticsearch_sg" {
  count       = var.create_vpc_access == "auto" || var.create_vpc_access == "manual" ? 1 : 0
  name        = "${var.name}-sg"
  description = "Allow https traffic"
  vpc_id      = var.create_vpc_access == "auto" ? aws_vpc.elasticsearch[0].id : var.vpc_id

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = var.sg_cidr
  }

  tags = {
    Name = "${var.name}-sg"
  }
}